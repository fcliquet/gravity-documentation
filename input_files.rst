=========================
Input files compatibility
=========================

This App was mainly developed while focusing on using `Gemini <http://gemini.readthedocs.io/en/latest/>`_ to store all the variants data, but it still allows for a basic treatment of VCF files. Taking in charge other file format should be easy to implement in the current API.

We advise using Gemini rather than the VCF file as an input.

------
Gemini
------

From our perspective, Gemini provide a flexible but standard database to store SNPs, INDELs and CNVs. It integrates directly the pedigree information, that we fully use, and can include additional columns to indicate more than one phenotype. (We have some work to do in order to allow for an easy customization of the App to adapt to variable format of phenotype columns)

It consists in a single sqlite file, that you will feed into the Cytoscape App.

---
VCF
---

We are reading VCF files containing multiple persons, but it is unclear whether it won't hit problems because of the annotations tolls used by the user (missing attributes, attributes that can't be stored/mapped, variations in the names of attributes). We are interested in keeping this compatibility, but at this stage of the development the priority is on the Gemini compatibility.

We do not manage external pedigree files for VCF, meaning that the user must manually select the corresponding parents for a patient, or that some options, sex related for instance, won't be available.
