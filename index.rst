.. App Documentation documentation master file, created by
   sphinx-quickstart on Mon May  2 15:49:19 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

App Documentation
=================

Contents:

.. toctree::
   :maxdepth: 2

   installation
   input_files
   controls
   results

   tutorial

Indices and tables
==================

* :ref:`search`
