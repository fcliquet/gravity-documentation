============
Installation
============

------------
Requirements
------------

* `Java 8 <http://www.java.com/>`_
* `Cytoscape 3.3.0 or later <http://www.cytoscape.org/download.php>`_


----------------
App installation
----------------

* Launch Cytoscape

If a previous version was installed, you need to remove it first:

* go to the menu *Apps*, *Apps Manager...* and then click on the *Currently installed* tab.
* Select the plugin and uninstall it

Install the new version:

* Download the App (`VCFviz-impl-1.0.0.jar <https://drive.google.com/file/d/0B5MbtgV8SfpFUFNucDl0YllaNkk/view?usp=sharing>`_)
* In Cytoscape, go to *Apps*, *Apps Manager...* and then click on the *Install from file...* button, then chose the *VCFviz-impl-1.0.0.jar* file you previously downloaded.

In Cytoscape *Control Panel* (menu on the left side), there is a tab called *Autism*. This is where you will control this App.
